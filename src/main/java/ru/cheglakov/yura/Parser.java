package ru.cheglakov.yura;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс парсинга XML файла
 *
 * @author yura.cheglakov
 * @version 2.0
 */

class Parser {
    private static String PATH;

    Parser(String path) {
        PATH = path;
    }

    /**
     * Метод парсинга XML файла
     *
     * @return словарь с книгами
     */
    Map<Integer, Map<String, String>> parse() throws ParserConfigurationException, IOException, SAXException {

        Map<Integer, Map<String, String>> mBook = new HashMap<>();

        final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document document = documentBuilder.parse(PATH);

        Node root = document.getDocumentElement();

        NodeList books = root.getChildNodes();
        for (int index = 0; index < books.getLength(); index++) {
            Node book = books.item(index);
            mBook.put(index, getChildes(book));
        }

        return mBook;
    }

    /**
     * Метод получения элементов из родительского элемента
     *
     * @param node элемент содержащий описание книги
     * @return словарь с описанием книг, ключ - параметр описания, значение - формулировка описания
     */

    private Map<String, String> getChildes(final Node node) {
        NodeList nodeList = node.getChildNodes();
        Map<String, String> mBookProps = new HashMap<>();
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node child = nodeList.item(index);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                mBookProps.put(child.getNodeName(), child.getTextContent());
            }
        }
        return mBookProps;
    }
}
