package ru.cheglakov.yura;

import com.itextpdf.text.DocumentException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Главный класс программы, принимает два обязательных аргумента <b>inputPath</b> и <b>outputPath</b>
 *
 * @author yura.cheglakov
 * @version 2.0
 */
public class XMLParser {

    private static final Logger log = Logger.getLogger(XMLParser.class.getName());
    private static final ArrayList<String> extension = new ArrayList<>();

    /**
     * Главный метод
     * @param args строка аргументов из командной строки
     */
    public static void main(String[] args) {
        if (!checkArgs(args)) {
            return;
        }

        try {
            new PDFGenerate(args[1]).pdfGenerate(new Parser(args[0]).parse());
        } catch (IOException exception) {
            log.log(Level.SEVERE, "Файл не найден", exception);
        } catch (DocumentException | SAXException | URISyntaxException exception) {
            log.log(Level.SEVERE, "Ошибка", exception);
        } catch (ParserConfigurationException exception) {
            log.log(Level.SEVERE, "Ошибка парсинга", exception);
        }
    }

    /**
     * Метод валидации аргументов запуска приложения
     * @param args строка аргументов из командной строки
     * @return вовзращает true, если с аргументами всё в порядке, иначе false
     */
    private static boolean checkArgs(String[] args) {
        if (args.length == 0) {
            log.log(Level.INFO, "Задайте входной файл и выходной файл. Например: java XMLParser books.xml books.pdf");
            return false;
        }

        for (String arg : args) {
            extension.add(arg.split("\\.")[1]);
        }

        if (extension.contains("xml") && extension.contains("pdf")) {
            return true;
        }

        log.log(Level.INFO, "Несоответствующий формат файла");
        return false;
    }

}
