package ru.cheglakov.yura;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Класс генерации PDF файла, принимает <b>outputPath</b> куда будет сохранен
 * @author yura.cheglakov
 * @version 2.0
 */
class PDFGenerate {
    private static final Document document = new Document(PageSize.A4, 30, 30, 50, 50);
    private final String outputPath;
    private final Font font = getFont();
    private static final String FONT_PATH = "/fonts/Arial.ttf";
    private static final String[] HEAD_LINES = {"Дата публикации", "Цена", "В наличии", "Жанр", "Автор", "Описание", "Название"};
    private static final PdfPTable table = new PdfPTable(7);

    PDFGenerate(String outputPath) throws DocumentException, IOException, URISyntaxException {
        this.outputPath = outputPath;
    }

    /**
     * Метод генерации pdf файла
     * @param books словарь, с параметрами книги
     * @throws IOException если данный путь не доступен
     * @throws DocumentException
     */

    void pdfGenerate(Map<Integer, Map<String, String>> books) throws IOException, DocumentException {

        try (FileOutputStream fileOutputStream = new FileOutputStream(outputPath)) {

            PdfWriter.getInstance(document, fileOutputStream);
            document.open();

            table.setSpacingBefore(25);

            for (String headline : HEAD_LINES) {
                table.addCell(new PdfPCell(new Phrase(headline, font)));
            }

            for (Map.Entry<Integer, Map<String, String>> entry : books.entrySet()) {
                for (Map.Entry<String, String> booksPropts : entry.getValue().entrySet()) {
                    if ("продано".equals(booksPropts.getKey())) {
                        continue;
                    }
                    table.addCell(new Phrase(booksPropts.getValue().trim(), font));
                }
            }
            document.add(new Paragraph("Список книжек", font));
            document.add(table);
            document.close();
        }
    }

    /**
     * Метод получения шрифта
     * @return вовзращает font, для отображения кириллицы в pdf
     * @throws URISyntaxException ошибка в пути к файлу
     * @throws IOException если по данному пути нет файла
     * @throws DocumentException
     */
    private Font getFont() throws URISyntaxException, IOException, DocumentException {
        return new Font(BaseFont.createFont(Paths.get(PDFGenerate.class.getResource(FONT_PATH).toURI()).toString(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
    }
}

